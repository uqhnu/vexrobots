# VexRobots
## Charge Battery
1. Take battery out on the left side of the brain
2. Charge with USB-C
3. Slide battery in where taken out

## Check battery status
Option 1: press small button on the battery on the left side of the brain
        1 light = 0-25% charge
        2 lights = 25-50% charge
        3 lights = 50-75% charge
        4 lights = 75-100% charge
        
Option 2: Start brain, status of the battery is displayed on the top right corner

## Starting the brain
Press the check-button

## Connect brain to the controller
1. Navigate to settings (4th position)
2. Start controller
3. Navigate to "Link" (2nd position)
4. Hold the L-buttons on the front of the controller and push the power button of the controller 2 times while holding the L-buttons
5. Navigate back to the home menu (cross-button)
6. Navigate to "Drive" (1st position)
7. Select the programm
** Drive: drives until battery has to be charged or cross is pressed
** 1-minute drive: Drives one minute
** 2-minutes drive: Drives two minutes

## Shut-down
Press corss-button 3 seconds

## Select a programm to drive
1. Navigate to Program (2nd position)
2. Select your prefered programm (8 different slots exist, not all are necessarily filled)
3. Select your prefered way of driving
** Drive: drives until battery has to be charged or cross is pressed
** 1-minute drive: Drives one minute
** 2-minutes drive: Drives two minutes

## Stop a programm
Press corss-button

## Test Sensors
Navigate to Devices (3rd position)
Gives you an overview what the current values are

# Sensors & Motors & Controller
## Controller Buttons
right and left joystick move right and left wheel
https://kb.vex.com/hc/en-us/sections/360007204612-Controller

Buttons Controller
https://kb.vex.com/hc/en-us/articles/360035592892-Understanding-Button-and-Joystick-Names-IQ-Controller

## Motors
Right motor: Port 1
Left motor: Prot 6
https://kb.vex.com/hc/en-us/articles/360035592972-Coding-with-VEX-IQ-Smart-Motors

## Bumper
Port 8
Is on the back of the robot and can detect if it is pressed.
https://kb.vex.com/hc/en-us/articles/360035955091-Using-the-VEX-IQ-Bumper-Switch

## TouchLED
Port 2
Is located next to Port 1
Can glow in different colors.
Can detect touch.
https://kb.vex.com/hc/en-us/articles/360035955091-Using-the-VEX-IQ-Bumper-Switch

## Distance Sensor
Port 7
Is located on the right front of the robot.
Can detect distance to the next object in mm and inches. After more than 2.5 meter the distance sensor thinks that there is something in between 10 and 20 cm in front of it.
Can detect the relative size of a object.
https://kb.vex.com/hc/en-us/articles/4407296307348-Using-the-IQ-Distance-Sensor-2nd-gen

## Optical Sensor
Port 3
Is located on the left front of the robot.
Can detect ligthness of a room.
Color detection.
https://kb.vex.com/hc/en-us/articles/4407229044500-Using-the-IQ-Optical-Sensor


# Current programms
## Slot 1 - Run and Turn
The robot drives until it the distance sensor detects an object in less than 20 cm. Then it turns right 10 degree and drives on.
If the TouchLED-Sensor is touched, the LED glows blue and the robot stops (until the distance sensor detects an object in less than 20 cm).

## Slot 2 - Run and run slower and Turn
The robot drives until it the distance sensor detects an object in less than 20 cm. Then it slows its speed down for 3 seconds. If an object is detected in less than 10 cm, the robot turns right 10 degree and drives on.
If the TouchLED-Sensor is touched, the LED glows blue and the robot stops (it turns if the distance sensor detects an object in less than 10cm).

## Slot 3 - Traffic Light
The robot drives until it find something red-ish (it needs to be only a few cm in front of the robot). If it is found it stops. If it is not there anymore then it starts again.

## Slot 4 - Drive Around
The robot drives until it detects an object in less than 10cm distance. Then it turns right 
If the TouchLED-Sensor is touched, the LED glows blue and the robot stops.

# Other
## Possibly useful links
Getting started
https://kb.vex.com/hc/en-us/categories/360002520052-Education

Link to all manuels
https://kb.vex.com/hc/en-us/categories/360002324792-IQ?sc=electronics

Link to controller
https://kb.vex.com/hc/en-us/sections/360007204612-Controller

Link to battery
https://kb.vex.com/hc/en-us/articles/4406898936724-Charging-the-VEX-IQ-2nd-gen-Battery

Build Instructions BeseBot
https://content.vexrobotics.com/stem-labs/iq/builds/basebot/iq-2nd-gen-basebot-with-sensors-rev13.pdf
